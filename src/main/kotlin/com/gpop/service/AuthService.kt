package com.gpop.service

import com.gpop.entity.Token
import com.gpop.request.LoginRequest
import com.gpop.request.RegisterRequest

interface AuthService {
    fun register(data: RegisterRequest): Token
    fun login(data: LoginRequest): Token
}