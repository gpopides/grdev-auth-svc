package com.gpop.service.implementation

import com.gpop.entity.Token
import com.gpop.exception.InternalServiceException
import com.gpop.repository.TokenRepository
import com.gpop.repository.UserRepository
import com.gpop.mapper.DtoMapper
import com.gpop.request.LoginRequest
import com.gpop.request.RegisterRequest
import com.gpop.service.AuthService
import org.mindrot.jbcrypt.BCrypt
import java.lang.Exception
import javax.enterprise.context.ApplicationScoped
import javax.transaction.Transactional
import javax.ws.rs.NotAuthorizedException

@ApplicationScoped
class AuthServiceImpl(
    private val userRepository: UserRepository,
    private val tokenRepository: TokenRepository,
    private val dtoMapper: DtoMapper
    ): AuthService {

    @Transactional
    override fun register(data: RegisterRequest): Token {
        val token = tokenRepository.create()
        val createUserDto = dtoMapper.createUserDto(data, ::encryptPassword)
        val internalUserId: Long? = userRepository.createInternalUser(dtoMapper.createUserRequest(createUserDto))

        internalUserId?.let {
            userRepository.create(internalUserId, createUserDto, token)
        } ?: throw InternalServiceException("could not create user")

        return token
    }

    override fun login(data: LoginRequest): Token {
        userRepository.findByEmail(data.email)?.let{
            if (!checkPassword(data.password, it.password)) {
                throw NotAuthorizedException("invalid_password")
            }

            val userToken = it.token

            return userToken.let { t -> tokenRepository.refreshAccessToken(t) }
        } ?: throw NotAuthorizedException("invalid_email")
    }


    private fun encryptPassword(pwd: String): String = BCrypt.hashpw(pwd, BCrypt.gensalt())
    private fun checkPassword(cleanPwd: String, hashedPwd: String): Boolean = BCrypt.checkpw(cleanPwd, hashedPwd)

    }
