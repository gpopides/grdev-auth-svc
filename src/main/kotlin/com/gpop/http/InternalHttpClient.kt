package com.gpop.http

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.time.Duration
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType

@ApplicationScoped
class InternalHttpClient: Client {
    private val http: HttpClient = HttpClient.newHttpClient()
    private val objectMapper = jacksonObjectMapper()

    override fun <T, R> post(url: URI, body: T, handler: (HttpResponse<String>) -> R): R {
        val request = HttpRequest.newBuilder(url)
            .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(body)))
            .timeout(Duration.ofMinutes(1))
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
            .build()


        val response = http.send(request, HttpResponse.BodyHandlers.ofString())
        return handler(response)
    }
}