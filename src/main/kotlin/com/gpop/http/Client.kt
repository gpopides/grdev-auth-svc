package com.gpop.http

import java.net.URI
import java.net.http.HttpResponse


interface Client {
    fun <T, R> post(url: URI, body: T, handler: (HttpResponse<String>) -> R): R
}
