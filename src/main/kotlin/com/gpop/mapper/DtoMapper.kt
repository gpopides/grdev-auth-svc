package com.gpop.mapper

import com.gpop.dto.CreateUserDto
import com.gpop.request.RegisterRequest
import com.gpop.request.internal.CreateUserRequest
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class DtoMapper {
    fun createUserDto(data: RegisterRequest, passwordEncryptor: (pwd: String) -> String): CreateUserDto {
        return CreateUserDto(
            email = data.email,
            password = passwordEncryptor(data.password),
            firstName = data.firstName,
            lastName = data.lastName
        )
    }

    fun createUserRequest(data: CreateUserDto): CreateUserRequest {
        return CreateUserRequest(
            firstName = data.firstName,
            lastName = data.lastName,
            email = data.email,
            organizationId = data.orgId
        )
    }
}