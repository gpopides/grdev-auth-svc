package com.gpop.controller

import com.gpop.request.LoginRequest
import com.gpop.request.RegisterRequest
import com.gpop.response.LoginResponse
import com.gpop.response.RegisterResponse
import com.gpop.service.AuthService
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("/api")
class AuthController(private val authService: AuthService) {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/register")
    fun register(data: RegisterRequest): RegisterResponse {
        val token = authService.register(data)

        return RegisterResponse(
            success = true,
            accessToken = token.accessToken,
            refreshToken = token.refreshToken
        )
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/login")
    fun login(data: LoginRequest): LoginResponse {
        return LoginResponse(true, authService.login(data).accessToken)
    }

}