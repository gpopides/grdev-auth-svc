package com.gpop.request

data class RegisterRequest(val email: String, val password: String, val firstName: String, val lastName: String, val orgId: Long? = null)
data class LoginRequest(val email: String, val password: String)
