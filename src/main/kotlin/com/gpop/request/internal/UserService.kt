package com.gpop.request.internal

data class CreateUserRequest(
    val firstName: String,
    val lastName: String,
    val email: String,
    val organizationId: Long? = null
)
