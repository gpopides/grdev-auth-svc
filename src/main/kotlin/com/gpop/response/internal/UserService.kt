package com.gpop.response.internal

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class UserDto(@JsonProperty("id") val id: Long)
data class CreateUserResponse(val success: Boolean, val error: String?, val data: UserDto?)
