package com.gpop.response

data class RegisterResponse(val success: Boolean, val accessToken: String, val refreshToken: String)
data class LoginResponse(val success: Boolean, val accessToken: String)
