package com.gpop.entity

import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "tokens")
class Token(
    @Column(name = "access_token")
    var accessToken: String,
    @Column(name = "refresh_token")
    var refreshToken: String,
    @Column(name = "expires_at")
    var expiresAt: LocalDateTime = LocalDateTime.now().plusHours(8)
    ) {
    constructor(): this("", "")

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long = 0

    @OneToOne(mappedBy="token")
    var user: User? = null
}
