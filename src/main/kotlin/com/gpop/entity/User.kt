package com.gpop.entity

import javax.persistence.*

@Entity
@Table(name = "users")
@IdClass(UserKey::class)
class User() {
    constructor(id: Long, email: String, password: String) : this() {
        this.email = email
        this.password = password
        this.userId = id
    }
    @Id
    @Column(name = "user_id")
    var userId: Long = 0

    @Column(name = "password")
    var password: String = ""

    @Column(name = "email")
    @Id
    var email: String = ""

    @OneToOne(cascade = [CascadeType.ALL])
    @JoinColumn(name = "token_id", referencedColumnName = "id")
    var token: Token = Token()
}
