package com.gpop.entity

import java.io.Serializable

class UserKey (val email: String = "", val userId: Long = 0): Serializable
