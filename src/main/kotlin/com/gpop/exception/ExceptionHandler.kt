package com.gpop.exception

import javax.ws.rs.NotAuthorizedException
import javax.ws.rs.ProcessingException
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class ExceptionHandler : ExceptionMapper<Throwable> {
    private val exceptionFactory: ExceptionFactory = ExceptionFactory()

    override fun toResponse(throwable: Throwable?): Response {
        println(throwable)
        return when (throwable) {
            is NotAuthorizedException -> Response.status(Response.Status.UNAUTHORIZED)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .entity(mapOf("error" to throwable.message, "success" to false))
                .build()
            is ProcessingException -> Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .entity(mapOf("error" to "could not connect to user svc"))
                .build()
            is InternalServiceException -> exceptionFactory.internalException(throwable)
            else -> throw throwable!!
        }
    }
}