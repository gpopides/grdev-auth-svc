package com.gpop.exception

import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.Response

@ApplicationScoped
class ExceptionFactory {
    fun httpException(exception: WebApplicationException): Response {
        val reason = exception.message ?: "something"
        val entity: Map<String, String> = mapOf("error" to reason)

        return Response
            .status(Response.Status.BAD_REQUEST)
            .entity(entity)
            .build()
    }

    fun internalException(exception: InternalServiceException): Response {
        val entity: Map<String, String?> = mapOf("error" to exception.message)

        return Response
            .status(Response.Status.BAD_REQUEST)
            .entity(entity)
            .build()
    }

}