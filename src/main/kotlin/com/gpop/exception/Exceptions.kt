package com.gpop.exception

import java.lang.RuntimeException

class InternalServiceException(msg: String): RuntimeException(msg)