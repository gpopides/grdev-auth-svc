package com.gpop.repository

import com.gpop.entity.Token
import com.gpop.entity.User
import com.gpop.dto.CreateUserDto
import com.gpop.request.RegisterRequest
import com.gpop.request.internal.CreateUserRequest
import io.quarkus.hibernate.orm.panache.PanacheRepository

interface UserRepository: PanacheRepository<User> {
    fun create(id: Long, dto: CreateUserDto, token: Token): User
    fun createInternalUser(data: CreateUserRequest): Long?
    fun findByEmail(email: String): User?
}