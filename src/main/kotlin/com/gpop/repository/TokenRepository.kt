package com.gpop.repository

import com.gpop.entity.Token
import io.quarkus.hibernate.orm.panache.PanacheRepository

interface TokenRepository: PanacheRepository<Token> {
    fun create(): Token
    fun refreshAccessToken(token: Token): Token
}