package com.gpop.repository.implementation

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.gpop.dto.CreateUserDto
import com.gpop.entity.Token
import com.gpop.entity.User
import com.gpop.repository.UserRepository
import com.gpop.http.Client
import com.gpop.request.internal.CreateUserRequest
import com.gpop.response.internal.CreateUserResponse
import org.slf4j.LoggerFactory
import java.net.URI
import javax.enterprise.context.ApplicationScoped
import javax.transaction.Transactional


@ApplicationScoped
class UserRepositoryImpl(private val client: Client): UserRepository {
    private val objectMapper = jacksonObjectMapper()
    private val logger = LoggerFactory.getLogger(UserRepositoryImpl::class.java)

    @Transactional
    override fun create(id: Long, data: CreateUserDto, token: Token): User {

       return User(id, data.email, data.password).let { user ->
            user.token = token
            persistAndFlush(user)
            user
        }
    }

    override fun createInternalUser(data: CreateUserRequest): Long? {
        return runCatching {
            client.post(URI.create("http://localhost:8080/api/user"), data) {
                val response: CreateUserResponse = objectMapper.readValue(it.body())
                response.data?.id
            }
        }
            .onFailure { e -> logger.error("Could not create user in user-svc.  Reason ${e.message}") }
            .getOrNull()
    }

    override fun findByEmail(email: String): User? {
        return find("email", email).firstResult()
    }

}
