package com.gpop.repository.implementation

import com.gpop.entity.Token
import com.gpop.repository.TokenRepository
import java.util.*
import javax.enterprise.context.ApplicationScoped
import javax.transaction.Transactional
import kotlin.random.Random

@ApplicationScoped
class TokenRepositoryImpl: TokenRepository {
    @Transactional
    override fun create(): Token {
        val accessToken = generateRandomString(40)
        val refreshToken = generateRandomString(40)

        return Token(accessToken, refreshToken).also {
            persistAndFlush(it)
        }
    }

    @Transactional
    override fun refreshAccessToken(token: Token): Token {
        generateRandomString(40).also {
            token.accessToken = it
            update("access_token = ?1 where id = ?2", it, token.id)
        }
        return token
    }

    private fun generateRandomString(size: Int): String {
        val charPool =  ('a'..'z') + ('A'..'Z') + ('0'..'9') + listOf('/', '+', '_')

        return (1..size)
            .map { Random.nextInt(0, charPool.size) }
            .map { i -> charPool[i] }
            .joinToString("")
    }
}