package com.gpop.dto

data class CreateUserDto(
    val email: String,
    val password: String,
    val firstName: String,
    val lastName: String,
    val orgId: Long? = null,
    )