
create table tokens (
                        id serial primary key,
                        refresh_token varchar(60) unique not null,
                        access_token varchar(60) unique not null,
                        expires_at timestamp
);
create table users (
    user_id int unique not null unique,
    password varchar(100) not null,
    email varchar(100) unique not null,
    token_id int references tokens(id),
    primary key (user_id, email)
);


CREATE SEQUENCE hibernate_sequence START 1;
