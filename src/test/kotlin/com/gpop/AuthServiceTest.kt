package com.gpop

import com.gpop.entity.Token
import com.gpop.entity.User
import com.gpop.repository.TokenRepository
import com.gpop.repository.UserRepository
import com.gpop.dto.CreateUserDto
import com.gpop.exception.InternalServiceException
import com.gpop.mapper.DtoMapper
import com.gpop.request.RegisterRequest
import com.gpop.request.internal.CreateUserRequest
import com.gpop.service.implementation.AuthServiceImpl
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class AuthServiceTest {
    private val userRepository: UserRepository = mockk()
    private val tokenRepository: TokenRepository = mockk()
    private val dtoMapper: DtoMapper = mockk()

    private val authServiceImpl = AuthServiceImpl(userRepository, tokenRepository, dtoMapper)

    @Test
    fun testRegisterSuccessful() {
        val token = mockk<Token>()
        val registerRequest = mockk<RegisterRequest>()
        val createUserDto = mockk<CreateUserDto>()
        val createUserRequest = mockk<CreateUserRequest>()
        val user = mockk<User>()
        val userId = 1L

        every { tokenRepository.create() }.returns(token)
        every { dtoMapper.createUserDto(registerRequest, any() ) }.returns(createUserDto)
        every { dtoMapper.createUserRequest(createUserDto) }.returns(createUserRequest)
        every { userRepository.createInternalUser(createUserRequest) }.returns(userId)
        every { userRepository.create(userId, createUserDto, token) }.returns(user)

        every { token.accessToken }.returns("encryptedtoken")
        every { token.refreshToken }.returns("encryptedtoken")

        val result = authServiceImpl.register(registerRequest)
        val expectedToken = Token("encryptedtoken", "encryptedtoken")

        assertEquals(expectedToken.refreshToken, result.refreshToken)
        assertEquals(expectedToken.accessToken, result.accessToken)
    }

    @Test
    fun testRegisterErrorInvalidResponseFromUserSvc() {
        val token = mockk<Token>()
        val registerRequest = mockk<RegisterRequest>()
        val createUserDto = mockk<CreateUserDto>()
        val createUserRequest = mockk<CreateUserRequest>()

        every { tokenRepository.create() }.returns(token)

        every { registerRequest.email }.returns("asd")
        every { registerRequest.firstName }.returns("asd")
        every { registerRequest.lastName }.returns("asd")
        every { registerRequest.password }.returns("asd")

        every { dtoMapper.createUserDto(registerRequest, any()) }.returns(createUserDto)

        every { dtoMapper.createUserRequest(createUserDto) }.returns(createUserRequest)

        every { userRepository.createInternalUser(createUserRequest) }.returns(null)

        assertThrows<InternalServiceException> {  authServiceImpl.register(registerRequest) }
    }
}